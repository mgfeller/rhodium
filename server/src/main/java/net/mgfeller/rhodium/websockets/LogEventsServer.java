package net.mgfeller.rhodium.websockets;

/*
 * #%L
 * server
 * %%
 * Copyright (C) 2014 Michael Gfeller
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * Author: Michael Gfeller
 */
@ServerEndpoint("/websockets/logevents")
public class LogEventsServer {

  private static Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());

  @OnOpen
  public void onOpen(final Session session) {
    sessions.add(session);
  }

  @OnClose
  public void onClose(final Session session) {
    sessions.remove(session);
  }

  @OnMessage
  public void broadcastLogEvent(final String text, final Session session) throws IOException {
    for (Session s : sessions) {
      if (!session.equals(s)) {
        s.getBasicRemote().sendText(text);
      }
    }
  }
}
