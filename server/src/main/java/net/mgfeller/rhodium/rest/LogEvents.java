package net.mgfeller.rhodium.rest;

/*
 * #%L
 * server
 * %%
 * Copyright (C) 2014 Michael Gfeller
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.mgfeller.rhodium.common.SuppressedWarnings;

import org.apache.log4j.Logger;

/**
 * Author: Michael Gfeller
 */
@Path(LogEvents.PATH)
@SuppressWarnings(SuppressedWarnings.UNUSED)
public class LogEvents {

  public final static String PATH = "/logevents";

  @Inject
  private Logger logger;

  @Inject
  private WebsocketsClientSessionProvider sessionProvider;

  @POST
  @Consumes({ MediaType.TEXT_PLAIN })
  public Response receiveLogEvent(final String logevent) {
    logger.info("Received: " + logevent);
    try {
      sessionProvider.getSession().getBasicRemote().sendText(logevent);
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }
    return Response.ok().build();
  }
}
